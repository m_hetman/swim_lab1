package com.example.bmi


import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun mainActivityTest() {
        val massEditText = onView(withId(R.id.massInput))
        massEditText.perform(click(), replaceText("65"), closeSoftKeyboard())

        val heightEditText = onView(withId(R.id.massInput))
        heightEditText.perform(click(), replaceText("110"), closeSoftKeyboard())

        val countButton = onView(withId(R.id.runButton))
        countButton.perform(click())

        val resultView = onView(withId(R.id.resView))
        resultView.check(matches(withText("53.719")))
    }
}
