package com.example.bmi

import android.content.Context
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.example.bmi.logic.BMI
import com.example.bmi.logic.BMI_forKgCm
import com.example.bmi.logic.BMI_forLbIn
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import java.text.DateFormat
import java.util.*
import kotlin.collections.HashMap
import kotlin.math.round

class MainActivity : AppCompatActivity() {
    companion object {
        const val BMI_LIMIT_1 = 18.5
        const val BMI_LIMIT_2 = 25
        const val BMI_LIMIT_3 = 30
        const val BMI_LIMIT_4 = 40

        fun getBmiColor(Bmi : Double) = when {
            Bmi < BMI_LIMIT_1  -> {
            Color.rgb(38,97,156) //lapis lazuli
            }
            Bmi < BMI_LIMIT_2-> {
                Color.rgb(67,179,174) //grynszpan
            }
            Bmi < BMI_LIMIT_3-> {
                Color.rgb(244,196,48) //szafranowy
            }
            Bmi < BMI_LIMIT_4-> {
                Color.rgb(233,116,81) //sjena palona
            }
            else -> {
                Color.rgb(184,0,0) // róż pompejańska
            }
        }

        internal fun usersBmi() = usersBmi
        private var usersBmi = 0.0
    }

    private var areUnitsChanged = false

    override fun onSaveInstanceState(outState: Bundle?) {
            super.onSaveInstanceState(outState)
            outState?.putDouble("usersBmi", usersBmi)
            outState?.putBoolean("units", areUnitsChanged)
    }

    override fun onPause() {
        super.onPause()
        saveHistoryData()
    }

    override fun onResume() {
        super.onResume()
        readHistoryData()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when {
            item?.itemId == R.id.change_units -> {
                clearInput()
                areUnitsChanged = !areUnitsChanged;
                setUnits()
                Toast.makeText(this, R.string.change_units_answ, Toast.LENGTH_LONG).show()
            }
            item?.itemId == R.id.history -> startActivity(Intent(this, History::class.java))
            item?.itemId == R.id.about -> startActivity(Intent(this, AboutMe::class.java))
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
           rebulidIO(savedInstanceState)
        runButton.setOnClickListener {
            if (massInput.text.isNotEmpty() && heightInput.text.isNotEmpty()) {
                if (massInput.text.toString().toInt() != 0 && heightInput.text.toString().toInt() != 0) {
                    runBmiCalc()
                } else Snackbar.make(it, R.string.wrong_data_com, Snackbar.LENGTH_LONG).show()
            } else Snackbar.make(it, R.string.incomp_data_com, Snackbar.LENGTH_LONG).show()
        }
    }

    private fun setResult(){
        resView.text = ((round(usersBmi * 1000))/1000).toString()

        val bmiDesc =
            when {
                usersBmi < BMI_LIMIT_1 -> R.string.bmi_range_1
                usersBmi < BMI_LIMIT_2 -> R.string.bmi_range_2
                usersBmi < BMI_LIMIT_3 -> R.string.bmi_range_3
                usersBmi < BMI_LIMIT_4 -> R.string.bmi_range_4
                else -> R.string.bmi_range_5
            }
        resDescView.text = resources.getString(bmiDesc)
        resView.setTextColor(getBmiColor(usersBmi))
        arrow.visibility = View.VISIBLE;
        arrow.setOnClickListener { startActivity(Intent(this, Info::class.java)) }
    }

    private fun setUnits(){
        if(areUnitsChanged) {
            massTxtView.text = resources.getString(R.string.mass) + BMI_forLbIn.MASS_UNIT
            heightTxtView.text = resources.getString(R.string.height) + BMI_forLbIn.HEIGHT_UNIT
        }
        else {
            massTxtView.text = resources.getString(R.string.mass) + BMI_forKgCm.MASS_UNIT
            heightTxtView.text = resources.getString(R.string.height) + BMI_forKgCm.HEIGHT_UNIT
        }
    }
    private fun clearInput() {
        massInput.text.clear()
        heightInput.text.clear()
        resView.text = ""
        resDescView.text = ""
        arrow.visibility = View.INVISIBLE;
        usersBmi = 0.0
    }
    private fun saveHistoryData() {
        val sharedPref = this.getPreferences(Context.MODE_PRIVATE)
        val editor = sharedPref.edit()

        editor.putInt("currIndex", History.currIndex)
        editor.putBoolean("isFull", History.isFull)
        for(i in 0..(History.LIST_SIZE -1)){
            editor.putInt("Mass$i", History.bmiList[i].mass())
            editor.putInt("Height$i", History.bmiList[i].height())
            editor.putString("Date$i", History.dateList[i])
            editor.putBoolean("Unit$i", History.unitsList[i])
        }
        editor.apply()
    }
    private fun readHistoryData() {
        val sharedPref = this.getPreferences(Context.MODE_PRIVATE)
        History.currIndex = sharedPref.getInt("currIndex",0)
        History.isFull = sharedPref.getBoolean("isFull", false)
        for(i in 0..(History.LIST_SIZE -1)){
            History.unitsList[i] = sharedPref.getBoolean("Unit$i", false)
            History.bmiList[i] =
                if(History.unitsList[i])
                    BMI_forLbIn(sharedPref.getInt("Mass$i", 0), sharedPref.getInt("Height$i", 0))
                else
                    BMI_forKgCm(sharedPref.getInt("Mass$i", 0), sharedPref.getInt("Height$i", 0))
            History.dateList[i] = sharedPref.getString("Date$i", "")
        }
    }
    private fun memorizeRecord(bmiCounter : BMI){
        History.bmiList[History.currIndex] = bmiCounter
        History.unitsList[History.currIndex] = areUnitsChanged
        History.dateList[com.example.bmi.History.currIndex] = DateFormat.getDateInstance().format(Calendar.getInstance().time)
        if(History.currIndex == History.LIST_SIZE - 1) {
            History.currIndex = 0
            History.isFull = true
        } else History.currIndex++
    }
    private fun runBmiCalc() {
        val mass : Int = massInput.text.toString().toInt()
        val height : Int = heightInput.text.toString().toInt()
        val bmiCounter : BMI

        if(areUnitsChanged) bmiCounter = BMI_forLbIn(mass, height)
        else bmiCounter = BMI_forKgCm(mass, height)
        usersBmi = bmiCounter.countBmi()
        setResult()
        memorizeRecord(bmiCounter)
    }
    private fun rebulidIO(savedInstanceState: Bundle?){
        if (savedInstanceState != null) {
            usersBmi = savedInstanceState.getDouble("usersBmi")
            areUnitsChanged = savedInstanceState.getBoolean("units")
            setUnits()
            if (usersBmi != 0.0)
                setResult()
        }
    }
}
