package com.example.bmi

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_info.*
import kotlin.math.round

class Info : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
        infoRes.text = "BMI:\n"+ (round(MainActivity.usersBmi() * 1000)/1000).toString()
        infoRes.setTextColor(MainActivity.getBmiColor(MainActivity.usersBmi()))
        when {
            MainActivity.usersBmi() < MainActivity.BMI_LIMIT_1 -> {
                niedowaga.visibility = View.VISIBLE
            }
            MainActivity.usersBmi() < MainActivity.BMI_LIMIT_2-> {
                prawidlowa.visibility = View.VISIBLE
            }
            MainActivity.usersBmi() < MainActivity.BMI_LIMIT_3 -> {
                nadwaga.visibility = View.VISIBLE
            }
            MainActivity.usersBmi() < MainActivity.BMI_LIMIT_4 -> {
                otylosc.visibility = View.VISIBLE
            }
            else -> {
                rip.visibility = View.VISIBLE
            }
        }

    }

}
