package com.example.bmi


import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class MainActivityTest2 {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun mainActivityTest2() {
        val massEditText = onView(withId(R.id.massInput))
        massEditText.perform(click(),replaceText("80"), closeSoftKeyboard())

        val heightEditText = onView(withId(R.id.heightInput))
        heightEditText.perform(click(),replaceText("180"), closeSoftKeyboard())

        val countButton = onView(withId(R.id.runButton))
        countButton.perform(click())

        val massText = onView(
            allOf(
                withId(R.id.massInput), withText("80")
            )
        )
        massText.check(matches(isDisplayed()))

        val heightText = onView(
            allOf(
                withId(R.id.heightInput), withText("80")
            )
        )
        massText.check(matches(isDisplayed()))
        val textView = onView(
            allOf(
                withId(R.id.resView), withText("24.691")
            )
        )
        textView.check(matches(isDisplayed()))
    }
}
