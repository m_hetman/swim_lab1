package com.example.bmi

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bmi.logic.BMI
import com.example.bmi.logic.BMI_forKgCm
import com.example.bmi.logic.BMI_forLbIn
import kotlinx.android.synthetic.main.activity_history.*
import java.util.*


class History : AppCompatActivity() {
    companion object {
        var isFull = false
        const val LIST_SIZE = 10
        var currIndex = 0
        val bmiList = Array<BMI>(LIST_SIZE) { BMI_forKgCm(0, 0) }
        val dateList = Array(LIST_SIZE) { "" }
        val unitsList = BooleanArray(LIST_SIZE)
    }

    private fun initRecylerView() {
        recyclerv_view.adapter = RecyclerViewAdapter(bmiList, unitsList, dateList)
        recyclerv_view.layoutManager = LinearLayoutManager(this)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)
        initRecylerView()
    }
}
