package com.example.bmi.logic

class BMI_forLbIn (var mass : Int, var height : Int) : BMI {
        companion object {
                val MASS_UNIT = " [lb]: "
                val HEIGHT_UNIT = " [in]: "
        }
        override fun countBmi() = mass * 703.0/ (height * height)
        override fun height(): Int = height
        override fun mass(): Int = mass
}