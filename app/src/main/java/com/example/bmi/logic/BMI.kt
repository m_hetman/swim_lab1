package com.example.bmi.logic

interface BMI {
    fun countBmi () : Double
    fun mass () : Int
    fun height () : Int
}