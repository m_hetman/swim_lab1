package com.example.bmi

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.bmi.logic.BMI
import com.example.bmi.logic.BMI_forKgCm
import com.example.bmi.logic.BMI_forLbIn
import kotlin.math.round

class RecyclerViewAdapter(val mBmis : Array<BMI>, val mUnits : BooleanArray, val mDates : Array<String> ) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_listitem, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return if(History.isFull) History.LIST_SIZE else History.currIndex
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.dateView.text = mDates[position]

        holder.massView.text = "Masa" +
                if(!mUnits[position]) BMI_forKgCm.MASS_UNIT
                else BMI_forLbIn.MASS_UNIT
                + mBmis[position].mass()

        holder.heightView.text = "Wzrost" +
                if(!mUnits[position]) BMI_forKgCm.HEIGHT_UNIT
                else BMI_forLbIn.HEIGHT_UNIT
                + mBmis[position].height()

        holder.bmiView.text = ((round(mBmis[position].countBmi() * 1000))/1000).toString()
        holder.bmiView.setTextColor(MainActivity.getBmiColor(mBmis[position].countBmi()))

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val dateView = itemView.findViewById<TextView>(R.id.dateHistView)
        val massView = itemView.findViewById<TextView>(R.id.massHistView)
        val heightView = itemView.findViewById<TextView>(R.id.heightHistView)
        val bmiView = itemView.findViewById<TextView>(R.id.bmiHistView)
    }
}