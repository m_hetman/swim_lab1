package com.example.bmi.logic

class BMI_forKgCm (var mass : Int, var height : Int) : BMI {
    companion object {
        val MASS_UNIT = " [kg] :"
        val HEIGHT_UNIT = " [cm] :"
    }
    override fun countBmi() = mass * 10000.0 / (height * height)
    override fun height(): Int = height
    override fun mass(): Int = mass
}
